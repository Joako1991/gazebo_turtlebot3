#!/bin/bash

path=$(dirname $(realpath $0))
cd $path
source devel/setup.bash
rosdep install -y --from-paths src --ignore-src --rosdistro melodic -r --os=debian:buster

